<?php

/**
 * @file
 * Contains \Drupal\nj_voter_list\Routing\RouteSubscriber.
 */

namespace Drupal\nj_voter_list\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Always deny access to field UI tabs.
    // Note that the second parameter of setRequirement() is a string.
    if ($route = $collection->get('entity.voter.field_ui_fields')) {
      $route->setRequirement('_access', 'FALSE');
    }
    if ($route = $collection->get('field_ui.field_storage_config_add_voter')) {
      $route->setRequirement('_access', 'FALSE');
    }
    if ($route = $collection->get('entity.entity_form_display.voter.default')) {
      $route->setRequirement('_access', 'FALSE');
    }
  }

  public static function getSubscribedEvents() {
    // Come after field_ui.
    $events[RoutingEvents::ALTER] = array('onAlterRoutes', -1100);
    return $events;
  }
}
