<?php

namespace Drupal\nj_voter_list;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Voter entities.
 *
 * @ingroup nj_voter_list
 */
interface VoterInterface extends ContentEntityInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Voter name.
   *
   * @return string
   *   Name of the Voter.
   */
  public function getFirstName();
  public function getMiddleName();
  public function getLastName();
  public function getDistrict();


}
