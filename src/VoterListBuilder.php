<?php

namespace Drupal\nj_voter_list;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Voter entities.
 *
 * @ingroup nj_voter_list
 */
class VoterListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Voter ID');
    $header['name'] = $this->t('Name');
    $header['party_code'] = $this->t('Party code');
    $header['municipality'] = $this->t('Municipality');
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\nj_voter_list\Entity\Voter */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.voter.canonical', array(
          'voter' => $entity->id(),
        )
      )
    );
    $row['party_code'] = $entity->getPartyCode();
    $row['municipality'] = $entity->get('municipality')->value;
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];

    $build['info'] = [
      '#markup' => 'todo:  count and stats on list',
    ];
    return $build + parent::render();
  }
}
