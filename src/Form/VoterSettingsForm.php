<?php

namespace Drupal\nj_voter_list\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Class VoterSettingsForm.
 *
 * @package Drupal\nj_voter_list\Form
 *
 * @ingroup nj_voter_list
 */
class VoterSettingsForm extends ConfigFormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'voter_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nj_voter_list.settings'];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('nj_voter_list.settings');
    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }


  /**
   * Defines the settings form for Voter entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['Voter_settings']['#markup'] = 'Settings form for Voter entities.';
    $form['default_municipal_filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default municipal filter'),
      '#description' => $this->t('Leave empty for no filter.'),
      '#default_value' => $this->config('nj_voter_list.settings')->get('default_municipal_filter'),
    ];
    $form['process_max_rows'] = [
      '#type' => 'number',
      '#title' => $this->t('How many rows to process at once when importing'),
      '#description' => $this->t('You may need to decrease this value if you get timeouts.'),
      '#default_value' => $this->config('nj_voter_list.settings')->get('process_max_rows'),
      '#required' => TRUE,
      '#min' => 10,
      '#max' => 500000,
      '#step' => 10,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $max = (int) $form_state->getValue('process_max_rows');
    $form_state->setValue('process_max_rows', $max);
  }
}
