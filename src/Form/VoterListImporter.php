<?php

namespace Drupal\nj_voter_list\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\nj_voter_list\VoterZipFileReader;

/**
 * Provides batch import
 */
class VoterListImporter {
  use DependencySerializationTrait;

  /**
   * The zip file reader.
   *
   * @var \Drupal\nj_voter_list\VoterZipFileReader
   */
  protected $vz;

  /**
   * @var string
   */
  protected $municipalFilter = '';

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a VoterListUploadForm object.
   *
   * @param \Drupal\nj_voter_list\VoterZipFileReader $vz
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param string $municipal_filter
   */
  public function __construct(VoterZipFileReader $vz, Connection $database, $municipal_filter = '') {
    $this->vz = $vz;
    $this->database = $database;
    $this->municipalFilter = $municipal_filter;
  }

  /**
   * Truncate the {voters} table.
   */
  public function truncate() {
    $this->database->truncate('voters')->execute();
  }

  /**
   * Save rows from the zip file int the database.
   *
   * @param int $offset
   *   An offset from the zip file from a previous ftell() call.
   * @param int $max_rows
   *   The maximum number of rows to process (even if not matched).
   *
   * @return int
   *   The number of rows inserted into the database.
   *
   * @throws \Drupal\nj_voter_list\VoterZipFileException
   * @throws \Exception
   */
  public function import($offset, $max_rows = 20000) {
    $count = 0;
    $rows = 0;
    $fields = $this->vz->fields();
    $this->vz->fseek($offset);
    do {
      $voter = $this->vz->fgetcsv();
      if ($voter) {
        $rows++;
        if ($this->municipalFilter && $voter['municipality'] !== $this->municipalFilter) {
          continue;
        }
        $this->database->insert('voters')
          ->fields($fields)
          ->values($voter)
          ->execute();
        $count++;
      }
    } while($voter && $rows < $max_rows);
    return $count;
  }

  /**
   * @return int
   */
  public function ftell() {
    return $this->vz->ftell();
  }

  /**
   * @return bool
   */
  public function eof() {
    return $this->vz->eof();
  }

  /**
   * @return array
   */
  public function stat() {
    return $this->vz->stat();
  }
}
