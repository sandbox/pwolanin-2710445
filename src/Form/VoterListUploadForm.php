<?php
/**
 * Contains \Drupal\nj_voter_list\Form\VoterListUploadForm.
 */

namespace Drupal\nj_voter_list\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\nj_voter_list\VoterZipFileReader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface;

/**
 * Class UploadVoterListForm.
 *
 * @package Drupal\nj_voter_list\Form
 */
class VoterListUploadForm extends FormBase {
  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a VoterListUploadForm object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   */
  public function __construct(FileSystemInterface $file_system, Connection $database) {
    $this->fileSystem = $file_system;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'voter_list_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['zip_upload'] = array(
      '#type' => 'file',
      '#title' => t('ZIP file upload'),
      '#description' => t("Upload the voter list from a county. e.g. AlphaVoterListReport/MERCER.zip"),
      //'#required' => TRUE,
    );
    $form['municipal_filter'] = array(
      '#type' => 'textfield',
      '#title' => t('Municipal filter'),
      '#default_value' => 'PRINCETON',
      '#maxlength' => 30,
    );
    $form['run'] = array(
      '#type' => 'submit',
      '#value' => t('Upload list'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (function_exists('file_save_upload')) {
      // Handle file uploads.
      $validators = array('file_validate_extensions' => array('zip'));

      // Check for a new uploaded favicon.
      $file = file_save_upload('zip_upload', $validators, FALSE, 0);
      if (!empty($file)) {
        // File upload was attempted.
        // Put the temporary file in form_values so we can save it on submit.
        $form_state->setValue('zip_upload', $file);
      }
      else {
        // File upload failed.
        $form_state->setErrorByName('zip_upload', $this->t('The file could not be uploaded.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $realpath = $this->fileSystem->realpath($values['zip_upload']->getFileUri());
    $vz = new VoterZipFileReader($realpath);

    $importer = new VoterListImporter($vz, $this->database, $values['municipal_filter']);
    $operations[] = [[static::class, 'processBatch'], [$importer]];
    $batch = [
      'operations' => $operations,
      'finished' => [static::class, 'finishBatch'],
      'title' => $this->t('Importing voter list'),
      'init_message' => $this->t('Starting voter list import.'),
      'error_message' => $this->t('Voter list import has encountered an error.'),
      'progress_message' => $this->t('@percentage% completed in @elapsed.'),
    ];
    batch_set($batch);
    $form_state->setRedirect('entity.voter.collection');
  }

  /**
   * Processes the list import batch.
   *
   * @param \Drupal\Core\Config\ConfigImporter $config_importer
   *   The batch config importer object to persist.
   * @param string $sync_step
   *   The synchronization step to do.
   * @param array $context
   *   The batch context.
   */

  /**
   * @param \Drupal\nj_voter_list\Form\VoterListImporter $importer
   * @param array $context
   */
  public static function processBatch(VoterListImporter $importer, array &$context) {
    if (!isset($context['sandbox']['offset'])) {
      $context['results']['count'] = 0;
      $context['sandbox']['offset'] = 0;
      $context['results']['errors'] = [];
      $context['finished'] = 0;
      $importer->truncate();
    }

    try {
      $context['results']['count'] += $importer->import($context['sandbox']['offset']);
      if ($importer->eof()) {
        $context['finished'] = 1;
      }
      else {
        $context['sandbox']['offset'] = $importer->ftell();
        $stat = $importer->stat();
        $context['finished'] = $context['sandbox']['offset'] / ($stat['size'] + 1);
      }
    }
    catch (\Exception $e) {
      $context['results']['errors'][] = $e->getMessage();
    }
  }

  /**
   * Finish batch.
   *
   * This function is a static function to avoid serializing the ConfigSync
   * object unnecessarily.
   */
  public static function finishBatch($success, $results, $operations) {
    if ($success) {
      if (!empty($results['errors'])) {
        foreach ($results['errors'] as $error) {
          drupal_set_message($error, 'error');
        }
      }
      $msg = new TranslatableMarkup('Inserted @count voters.', ['@count' => $results['count']]);
      drupal_set_message($msg);
    }
    else {
      $msg = new TranslatableMarkup('Failure inserting voters.');
      drupal_set_message($msg, 'error');
    }
  }

}
