<?php

namespace Drupal\nj_voter_list\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\nj_voter_list\VoterInterface;

/**
 * Defines the Voter entity.
 *
 * @ingroup nj_voter_list
 *
 * @ContentEntityType(
 *   id = "voter",
 *   label = @Translation("Voter"),
 *   handlers = {
 *     "storage_schema" = "Drupal\nj_voter_list\VoterStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\nj_voter_list\VoterListBuilder",
 *     "views_data" = "Drupal\nj_voter_list\Entity\VoterViewsData",

 *     "access" = "Drupal\nj_voter_list\VoterAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\nj_voter_list\VoterHtmlRouteProvider",
 *     },
 *   },
 *   persistent_cache = FALSE,
 *   base_table = "voters",
 *   admin_permission = "administer voter entities",
 *   entity_keys = {
 *     "id" = "voter_id",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/voter/{voter}",
 *     "collection" = "/admin/structure/voter",
 *   },
 *   field_ui_base_route = "entity.voter.collection"
 * )
 */
class Voter extends ContentEntityBase implements VoterInterface {

  /**
   * {@inheritdoc}
   */
  public function getFirstName() {
    return $this->get('first_name')->value;
  }

  public function getMiddleName() {
    return $this->get('middle_name')->value;
  }

  public function getLastName() {
    return $this->get('last_name')->value;
  }

  public function getDistrict() {
    return $this->get('district')->value;
  }

  public function getPartyCode() {
    return $this->get('party_code')->value;
  }

  public function label() {
    return $this->getFirstName() . ' ' . $this->getLastName();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['county'] = BaseFieldDefinition::create('string')
      ->setLabel('County')
      ->setDescription(t('The county of the Voter.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 120,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['voter_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Voter entity.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => -10,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['last_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Last Name'))
      ->setDescription(t('The last name of the Voter.'))
      ->setSettings(array(
        'max_length' => 40,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['first_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('First Name'))
      ->setDescription(t('The first name of the Voter.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 5,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['middle_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Middle Name'))
      ->setDescription(t('The middle name of the Voter.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 15,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['suffix'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name suffix'))
      ->setDescription(t('The name suffix of the Voter.'))
      ->setSettings(array(
        'max_length' => 5,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 20,
      ))
      ->setDisplayConfigurable('view', TRUE);


    $fields['street_number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Street number'))
      ->setDescription(t('The street number of the Voter.'))
      ->setSettings(array(
        'max_length' => 20,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 25,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['suffix_a'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Street number suffix A'))
      ->setDescription(t('Street number suffix A.'))
      ->setSettings(array(
        'max_length' => 8,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 30,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['suffix_b'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Street number suffix B'))
      ->setDescription(t('Street number suffix B.'))
      ->setSettings(array(
        'max_length' => 4,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 35,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['street_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Street name'))
      ->setDescription(t('The street name of the Voter.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 40,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['apt_unit_no'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Apt no'))
      ->setDescription(t('Apartment or unit number.'))
      ->setSettings(array(
        'max_length' => 8,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 45,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['city'] = BaseFieldDefinition::create('string')
      ->setLabel(t('City'))
      ->setDescription(t('The city of the Voter.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 50,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['municipality'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Municipality'))
      ->setDescription(t('The municipality of the Voter.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 55,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['zip5'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ZIP code'))
      ->setDescription(t('The zip code of the Voter.'))
      ->setSettings(array(
        'max_length' => 5,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 60,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['date_of_birth'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Date of birth'))
      ->setDescription(t('The date of birth of the Voter (as ISO 8601 string).'))
      ->setSettings(array(
        'max_length' => 10,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 65,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['party_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Party code'))
      ->setDescription(t('The party code of the Voter.'))
      ->setSettings(array(
        'max_length' => 5,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 70,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['ward'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Ward'))
      ->setDescription(t('The ward of the Voter.'))
      ->setSettings(array(
        'max_length' => 2,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 75,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['district'] = BaseFieldDefinition::create('string')
      ->setLabel(t('District'))
      ->setDescription(t('The district of the Voter.'))
      ->setSettings(array(
        'max_length' => 2,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 80,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of the Voter.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 85,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['congressional'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Congressional District'))
      ->setDescription(t('The congressional district of the Voter.'))
      ->setSettings(array(
        'max_length' => 4,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 90,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['legislative'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Legislative District'))
      ->setDescription(t('The legislative district of the Voter.'))
      ->setSettings(array(
        'max_length' => 4,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 95,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['school'] = BaseFieldDefinition::create('string')
      ->setLabel(t('School District'))
      ->setDescription(t('The school district of the Voter.'))
      ->setSettings(array(
        'max_length' => 8,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 95,
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['street_num_int'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Street number as integer'))
      ->setDescription(t('The street number of the Voter.'))
      ->setDefaultValue(0);

    $fields['door'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Door'))
      ->setDescription(t('The normalized door for grouping.'))
      ->setSettings(array(
        'max_length' => 130,
        'text_processing' => 0,
      ))
      ->setDefaultValue('');

    return $fields;
  }

}
