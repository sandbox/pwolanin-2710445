<?php

namespace Drupal\nj_voter_list\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Voter entities.
 */
class VoterViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['voters']['table']['base'] = array(
      'field' => 'voter_id',
      'title' => $this->t('Voter'),
      'help' => $this->t('The Voter ID.'),
    );

    return $data;
  }

}
