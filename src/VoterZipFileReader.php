<?php

/**
 * @file
 * Contains \Drupal\nj_voter_list\VoterZipFileReader.
 */

namespace Drupal\nj_voter_list;

/**
 * Reads voter records from a zip file.
 */
class VoterZipFileReader implements VoterZipFileReaderInterface {

  /**
   * @var array
   */
  protected static $fieldsMap = [
    'county' => 0,
    'voter_id' => 1,
    'last_name' => 3,
    'first_name' => 4,
    'middle_name' => 5,
    'suffix' => 6,
    'street_number' => 7,
    'suffix_a' => 8,
    'suffix_b' => 9,
    'street_name' => 10,
    'apt_unit_no' => 11,
    'city' => 12,
    'municipality' => 13,
    'zip5' => 14,
    'date_of_birth' => 15,
    'party_code' => 16,
    'ward' => 17,
    'district' => 18,
    'status' => 19,
    'congressional' => 20,
    'legislative' => 21,
    'school' => 23,
    'street_num_int' => 26,
    'door' => 27,
  ];

  /**
   * Index values here must be in sync with static::$fieldsMap
   *
   * @var array
   */
  protected static $doorFields = [
    'street_name' => 10,
    'street_number' => 7,
    'suffix_a' => 8,
    'suffix_b' => 9,
    'apt_unit_no' => 11,
    'municipality' => 13,
    'zip5' => 14,
  ];

  /**
   * @var string
   */
  protected $fileName;

  /**
   * @var string
   */
  protected $zipFilePath;

  /**
   * @var string
   */
  protected $tempDir;

  /**
   * @var resource
   */
  protected $fp;

  /**
   * @var boolean
   */
  protected $eof = FALSE;

  /**
   * @var boolean
   */
  protected $exception = FALSE;

  /**
   * VoterZipFileReader constructor.
   * @param string $zip_file_path
   *   The path or uri to a zip archive.
   * @param string $file_name
   *   The name of the file in the zip archive to use.
   */
  function __construct($zip_file_path, $file_name = 'AlphaVoterListState.txt') {
    $this->fileName = $file_name;
    $this->zipFilePath = $zip_file_path;
    $this->tempDir = sys_get_temp_dir() . '/VoterZipFileReader' . uniqid(mt_rand(), TRUE);
  }

  /**
   * Magic method for serialize.
   *
   * @return array
   */
  public function __sleep() {
    return ['fileName', 'zipFilePath', 'tempDir', 'eof'];
  }

  /**
   * Magic method to clean up when going out of scope.
   */
  public function __destruct() {
    if ($this->fp) {
      fclose($this->fp);
    }
    if ($this->eof || $this->exception) {
      // Clean up the extracted file.
      $this->cleanup();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function cleanup() {
    $filepath = $this->tempDir . '/' . $this->fileName;
    if (is_file($filepath)) {
      unlink($filepath);
    }
    if (is_dir($this->tempDir)) {
      rmdir($this->tempDir);
    }
  }

  /**
   * @param string $msg
   * @param int $code
   * @throws \Drupal\nj_voter_list\VoterZipFileException
   */
  protected function throwException($msg, $code = 0) {
    $this->exception = TRUE;
    throw new VoterZipFileException($msg, $code);
  }

  /**
   * {@inheritdoc}
   */
  public function open() {
    if (empty($this->fp)) {
      if (!is_dir($this->tempDir)) {
        mkdir($this->tempDir, 0700);
      }
      $filepath = $this->tempDir . '/' . $this->fileName;
      if (!is_file($filepath)) {
        $zip = new \ZipArchive();
        $res = $zip->open($this->zipFilePath);
        if ($res !== TRUE) {
          $this->throwException(sprintf('Error while opening: %s', $this->zipFilePath), $res);
        }
        if (!$zip->extractTo($this->tempDir, $this->fileName)) {
          $this->throwException(sprintf('Error while extracting file: %s in %s', $this->fileName, $this->zipFilePath));
        }
      }
      $setting = ini_get('auto_detect_line_endings');
      ini_set('auto_detect_line_endings', TRUE);
      $this->fp = fopen($filepath, 'r');
      if ($this->fp === FALSE) {
        $this->throwException(sprintf('Error while opening file: %s', $filepath));
      }
      // Restore setting.
      ini_set('auto_detect_line_endings', $setting);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function ftell() {
    $this->open();
    return ftell($this->fp);
  }

  /**
   * {@inheritdoc}
   */
  public function stat() {
    $this->open();
    return fstat($this->fp);
  }

  /**
   * {@inheritdoc}
   */
  public function fseek($offset) {
    $this->open();
    if (fseek($this->fp, $offset) !== 0) {
      $this->throwException(sprintf('Error seeking offset %d in %s in %s', $offset, $this->fileName, $this->zipFilePath));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fgetcsv() {
    $this->open();
    // Skip up to 2 non-data lines that occur usually at the end of the file.
    $try = 0;
    do {
      $line = fgetcsv($this->fp, 300, '|');
      $try++;
    } while (is_array($line) && $try < 3 && count($line) < 2);
    if ($line === FALSE) {
      $this->eof = TRUE;
      if (!feof($this->fp)) {
        $this->throwException("Unexpected fgetcsv() failure reading file");
      }
      return FALSE;
    }
    return static::buildVoterFields($line);
  }

  /**
   * {@inheritdoc}
   */
  public static function fields() {
    return array_keys(static::$fieldsMap);
  }

  /**
   * {@inheritdoc}
   */
  public function eof() {
    return $this->eof;
  }

  /**
   * Re-format an American date to ISO8601
   *
   * @param $str
   *
   * @return string
   */
  protected static function reformatDate($str) {
    $parts = explode('/', $str);
    return "{$parts[2]}-{$parts[0]}-{$parts[1]}";
  }

  /**
   * {@inheritdoc}
   */
  public function buildVoterFields(array $line) {
    if (count($line) != self::EXPECTED_NUM_FIELDS) {
      $this->throwException(sprintf('Invalid line. Found %d fields, expected %d', count($line), self::EXPECTED_NUM_FIELDS));
    }
    $voter = [];
    $map = static::$fieldsMap;
    $line[$map['street_num_int']] = intval($line[$map['street_number']]);
    // Truncate zip5 to actually 5 digits (some looked like '085423347').
    $line[$map['zip5']] = substr($line[$map['zip5']], 0, 5);
    $door = [];
    foreach (self::$doorFields as $idx) {
      $door[] = $line[$idx];
    }
    // Use a character that sorts before numbers and letters.
    $line[$map['door']] = implode('!', $door);
    foreach ($map as $key => $idx) {
      $voter[$key] = $line[$idx];
    }
    $voter['date_of_birth'] = static::reformatDate($voter['date_of_birth']);
    return $voter;
  }
}
