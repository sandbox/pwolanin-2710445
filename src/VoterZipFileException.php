<?php

/**
 * @file
 * Contains \Drupal\nj_voter_list\VoterZipFileException.
 */

namespace Drupal\nj_voter_list;

/**
 * Defines a custom exception.
 */
class VoterZipFileException extends \Exception {

  public function __construct($message = '', $code = 0, $previous = NULL) {
    if ($code && ($msg = static::mapCode($code))) {
      $message = $msg . ' ' . $message;
    }
    parent::__construct($message, $code, $previous);
  }

  /**
   * Map codes to messages based on http://php.net/manual/en/ziparchive.open.php
   *
   * @param int $code
   *
   * @return string
   */
  protected static function mapCode($code) {
    switch ($code) {
      case \ZipArchive::ER_EXISTS:
        return 'File already exists.';
      case \ZipArchive::ER_INCONS:
        return 'Zip archive inconsistent.';
      case \ZipArchive::ER_INVAL:
        return 'Invalid argument.';
      case \ZipArchive::ER_MEMORY:
        return 'Malloc failure.';
      case \ZipArchive::ER_NOENT:
        return 'No such file.';
      case \ZipArchive::ER_NOZIP:
        return 'Not a zip archive.';
      case \ZipArchive::ER_OPEN:
        return 'Can not open file.';
      case \ZipArchive::ER_READ:
        return 'Read error.';
      case \ZipArchive::ER_SEEK:
        return 'Seek error.';
      default:
        return '';
    }
  }
}
