<?php

/**
 * @file
 * Contains \Drupal\nj_voter_list\VoterZipFileReaderInterface.
 */

namespace Drupal\nj_voter_list;

/**
 * Defines the voter schema handler.
 */
interface VoterZipFileReaderInterface {

  // The number of expected fields in a NJ SVRS pipe-delimited line.
  const EXPECTED_NUM_FIELDS = 26;

  /**
   * Open the file, if not already open.
   *
   * @throws \Drupal\nj_voter_list\VoterZipFileException
   *   If there was an error opening the file.
   */
  public function open();

  /**
   * Remove any files that were created.
   */
  public function cleanup();

  /**
   * Find the offset into the file stream.
   *
   * Opens the file, if not already open.
   *
   * @return int
   *   The current offset into the file stream.
   *
   * @throws \Drupal\nj_voter_list\VoterZipFileException
   *   If there was an error opening the file.
   */
  public function ftell();

  /**
   * Get information about the file being processed.
   *
   * Opens the file, if not already open.
   *
   * @return array
   *   The file information from \ZipArchive::statName().
   *
   * @throws \Drupal\nj_voter_list\VoterZipFileException
   *   If there was an error opening the file.
   */
  public function stat();

  /**
   * Seek to the absolute offset in bytes into the file stream.
   *
   * Opens the file, if not already open.
   *
   * Important - this is a relative seek, so you should subtract the value
   * of ::ftell() from the offset if you need an absolute position.
   *
   * @param int $offset
   *
   * @throws \Drupal\nj_voter_list\VoterZipFileException
   *   If there was an error opening or seeking the file.
   */
  public function fseek($offset);

  /**
   * Read a line of the file into a keyed array. Some values may be computed.
   *
   * Opens the file, if not already open.
   *
   * @return array|FALSE
   *   The current voter record. The array keys will match self::fields(). FALSE
   *   if the end of file was reached or an error reading the file.
   *
   * @throws \Drupal\nj_voter_list\VoterZipFileException
   *   If there was an error opening the file.
   */
  public function fgetcsv();

  /**
   * The voter record fields. Should match the Voter entity schema.
   *
   * @todo Make this an interface constant when PHP 7 is required.
   *
   * @return array
   *   The voter record fields returned by self::fgetcsv().
   */
  public static function fields();

  /**
   * TRUE if the file has been opened and the end of file reached self::fgescsv().
   *
   * @return bool
   */
  public function eof();

  /**
   * Helper function to convert a numerically indexed line to associative array.
   * @param array $line
   * @return array
   *
   * @throws \Drupal\nj_voter_list\VoterZipFileException
   *   If the line does not match the expected number of fields or format.
   */
  public function buildVoterFields(array $line);
}
