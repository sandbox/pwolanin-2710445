<?php

/**
 * @file
 * Contains \Drupal\nj_voter_list\VoterStorageSchema.
 */

namespace Drupal\nj_voter_list;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the voter schema handler.
 */
class VoterStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    // Since the Voter ID is from external data, don't make this a serial field.
    $schema['voters']['fields']['voter_id']['type'] = 'int';

    $schema['voters']['indexes'] += array(
      'voters__door' => array('door'),
      'voters__last_name' => array('last_name'),
      'voters__street_num' => array(
        'street_name',
        'street_num_int',
        'suffix_a',
        'suffix_b',
        'apt_unit_no',
      ),
    );

    return $schema;
  }

}
