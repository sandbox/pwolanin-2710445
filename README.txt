Copyright (c) 2016 by Peter Wolanin.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 2 or any later version.

This module is intended to read, parse, and bulk-load NJ state
voter list records from the NJ Division of Elections.

A custom conent entity is used to represent each voter to allow
for easier customization of display and to allow the use of
entity reference fields to link e.g. a Democratic club membership
record (a node or other entity) with a voter record.

The data in the voter list while a matter of public record, it should
not be (as a matter of courtesy) published to the web for anonymous
users. Thus the "View Voter entities" permission should never
be given to anonymous users.

