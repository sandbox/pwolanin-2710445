<?php

/**
 * @file
 * Contains \Drupal\nj_voter_list\VoterZipFileReaderReadTest.
 */

namespace Drupal\Tests\nj_voter_list\Unit;

use Drupal\nj_voter_list\VoterZipFileReaderInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\nj_voter_list\VoterZipFileReader;
use Drupal\nj_voter_list\VoterZipFileException;

/**
 * Reads voter records from a zip file.
 * @coversDefaultClass \Drupal\nj_voter_list\VoterZipFileReader
 */
class VoterZipFileReaderReadTest extends UnitTestCase {


  /**
   * @covers ::fields
   */
  public function testFieldsList() {
    $fields = VoterZipFileReader::fields();
    $expected = [
      'county',
      'voter_id',
      'last_name',
      'first_name',
      'middle_name',
      'suffix',
      'street_number',
      'suffix_a',
      'suffix_b',
      'street_name',
      'apt_unit_no',
      'city',
      'municipality',
      'zip5',
      'date_of_birth',
      'party_code',
      'ward',
      'district',
      'status',
      'congressional',
      'legislative',
      'school',
      'street_num_int',
      'door',
    ];
    $this->assertSame($expected, $fields);
  }

  /**
   * @covers ::fseek
   */
  public function testFileSeek() {
    $v = new VoterZipFileReader(__DIR__ . '/../../fixtures/MERCER_25.zip');
    $this->assertSame(0, $v->ftell());
    $v->fseek(100);
    $this->assertSame(100, $v->ftell());
    $v->fseek(50);
    $this->assertSame(50, $v->ftell());
    $v->fseek(300);
    $this->assertSame(300, $v->ftell());
    $v->cleanup();
  }

  /**
   * @covers ::fseek
   * @covers ::fgetcsv
   * @dataProvider seekLineProvider
   */
  public function testFile($seek, $expected_voter, $tell) {
    $v = new VoterZipFileReader(__DIR__ . '/../../fixtures/MERCER_25.zip');
    $v->fseek($seek);
    $this->assertSame($expected_voter, $v->fgetcsv());
    $this->assertSame($tell, $v->ftell());
    $v->cleanup();
  }

  /**
   * Data provider
   *
   * @return array
   */
  public function seekLineProvider() {
    return [
      [ 280,
        ['county' => 'MERCER',
        'voter_id' => '441850088',
        'last_name' => 'ADAMS',
        'first_name' => 'MARISA',
        'middle_name' => 'E',
        'suffix' => '',
        'street_number' => '188',
        'suffix_a' => '',
        'suffix_b' => '',
        'street_name' => 'DUTCH NECK RD',
        'apt_unit_no' => 'L7',
        'city' => 'EAST WINDSOR',
        'municipality' => 'EAST WINDSOR',
        'zip5' => '08520',
        'date_of_birth' => '1980-11-17',
        'party_code' => 'DEM',
        'ward' => '00',
        'district' => '14',
        'status' => 'Active',
        'congressional' => '12',
        'legislative' => '14',
        'school' => '60.704',
        'street_num_int' => 188,
        'door' => 'DUTCH NECK RD!188!!!L7!EAST WINDSOR!08520',],
        414,
      ],
      [ 650,
        [ 'county' => 'MERCER',
          'voter_id' => '430857556',
          'last_name' => 'BLESSING',
          'first_name' => 'PAUL',
          'middle_name' => 'W',
          'suffix' => '',
          'street_number' => '175',
          'suffix_a' => '',
          'suffix_b' => '',
          'street_name' => 'ELLSWORTH DR',
          'apt_unit_no' => '',
          'city' => 'WEST WINDSOR',
          'municipality' => 'WEST WINDSOR',
          'zip5' => '08550',
          'date_of_birth' => '1965-05-11',
          'party_code' => 'REP',
          'ward' => '00',
          'district' => '03',
          'status' => 'Active',
          'congressional' => '12',
          'legislative' => '15',
          'school' => '60.804',
          'street_num_int' => 175,
          'door' => 'ELLSWORTH DR!175!!!!WEST WINDSOR!08550',],
       782,
      ]
    ];
  }

  /**
   * Read all the lines from a file.
   *
   * @covers ::eof
   * @covers ::fgetcsv
   */
  public function testReadFullFile() {
    $v = new VoterZipFileReader(__DIR__ . '/../../fixtures/MERCER_25.zip');
    $found = 0;
    do {
      $this->assertSame(FALSE, $v->eof());
      $voter = $v->fgetcsv();
      if ($voter) {
        $found++;
      }
    } while($voter);
    $this->assertSame(25, $found, 'Found expected number of voters');
    $this->assertSame(TRUE, $v->eof());
    $v->cleanup();
  }
}
