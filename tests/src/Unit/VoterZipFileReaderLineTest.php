<?php

/**
 * @file
 * Contains \Drupal\nj_voter_list\VoterZipFileReaderLineTest.
 */

namespace Drupal\Tests\nj_voter_list\Unit;

use Drupal\nj_voter_list\VoterZipFileReaderInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\nj_voter_list\VoterZipFileReader;

/**
 * Reads voter records from a zip file.
 * @coversDefaultClass \Drupal\nj_voter_list\VoterZipFileReader
 */
class VoterZipFileReaderLineTest extends UnitTestCase {

  /**
   * @covers ::open
   * @expectedException \Drupal\nj_voter_list\VoterZipFileException
   * @expectedExceptionCode \ZipArchive::ER_OPEN
   */
  public function testMissingZipFile() {
    $v = new VoterZipFileReader(__DIR__ . '/missing.zip');
    $v->open();
  }

  /**
   * @covers ::open
   * @expectedException \Drupal\nj_voter_list\VoterZipFileException
   * @expectedExceptionCode 0
   * @expectedExceptionMessage Error while extracting file
   */
  public function testMissingTxtFile() {
    $v = new VoterZipFileReader(__DIR__ . '/../../fixtures/MERCER_25.zip', 'MISSING.txt');
    $v->open();
  }

  /**
   * Data provider
   *
   * @return array
   */
  public function invalidLineProvider() {
    $data = [
      [[]], // 0
      [['MERCER']], // 1
      [['MERCER', '666746496']], // 2
    ];
    for ($count = 1; $count < VoterZipFileReaderInterface::EXPECTED_NUM_FIELDS + 5; $count++) {
      if ($count == VoterZipFileReaderInterface::EXPECTED_NUM_FIELDS) {
        continue;
      }
      $data[] = [array_fill(0, $count, '')];
    }
    return $data;
  }

  /**
   * @covers ::buildVoterFields
   * @dataProvider invalidLineProvider
   * @expectedException \Drupal\nj_voter_list\VoterZipFileException
   * @expectedExceptionMessage Invalid line
   */
  public function testInvalidLine($line) {
    $v = new VoterZipFileReader('fake.zip');
    $v->buildVoterFields($line);
  }

  /**
   * Data provider
   *
   * @return array
   */
  public function validLineProvider() {
    return [
      [[
        0 => 'MERCER',
        1 => '441850088',
        2 => '',
        3 => 'ADAMS',
        4 => 'MARISA',
        5 => 'E',
        6 => '',
        7 => '188',
        8 => '',
        9 => '',
        10 => 'DUTCH NECK RD',
        11 => 'L7',
        12 => 'EAST WINDSOR',
        13 => 'EAST WINDSOR',
        14 => '08520',
        15 => '11/17/1980',
        16 => 'DEM',
        17 => '00',
        18 => '14',
        19 => 'Active',
        20 => '12',
        21 => '14',
        22 => '',
        23 => '60.704',
        24 => '',
        25 => '',
      ],
      [
        'county' => 'MERCER',
        'voter_id' => '441850088',
        'last_name' => 'ADAMS',
        'first_name' => 'MARISA',
        'middle_name' => 'E',
        'suffix' => '',
        'street_number' => '188',
        'suffix_a' => '',
        'suffix_b' => '',
        'street_name' => 'DUTCH NECK RD',
        'apt_unit_no' => 'L7',
        'city' => 'EAST WINDSOR',
        'municipality' => 'EAST WINDSOR',
        'zip5' => '08520',
        'date_of_birth' => '1980-11-17',
        'party_code' => 'DEM',
        'ward' => '00',
        'district' => '14',
        'status' => 'Active',
        'congressional' => '12',
        'legislative' => '14',
        'school' => '60.704',
        'street_num_int' => 188,
        'door' => 'DUTCH NECK RD!188!!!L7!EAST WINDSOR!08520',
      ]],
      [[
        0 => 'MERCER',
        1 => '551850078',
        2 => '321123',
        3 => 'ADAMS',
        4 => 'JOHN',
        5 => 'QUINCY',
        6 => 'JR',
        7 => '1776',
        8 => '',
        9 => '1/2',
        10 => 'BATTLE RD',
        11 => '',
        12 => 'PRINCETON',
        13 => 'WEST WINDSOR',
        14 => '085404321',
        15 => '01/27/1970',
        16 => 'UNA',
        17 => '00',
        18 => '07',
        19 => 'Active',
        20 => '12',
        21 => '15',
        22 => '',
        23 => '60.603',
        24 => '',
        25 => '',
      ],
      [
        'county' => 'MERCER',
        'voter_id' => '551850078',
        'last_name' => 'ADAMS',
        'first_name' => 'JOHN',
        'middle_name' => 'QUINCY',
        'suffix' => 'JR',
        'street_number' => '1776',
        'suffix_a' => '',
        'suffix_b' => '1/2',
        'street_name' => 'BATTLE RD',
        'apt_unit_no' => '',
        'city' => 'PRINCETON',
        'municipality' => 'WEST WINDSOR',
        'zip5' => '08540',
        'date_of_birth' => '1970-01-27',
        'party_code' => 'UNA',
        'ward' => '00',
        'district' => '07',
        'status' => 'Active',
        'congressional' => '12',
        'legislative' => '15',
        'school' => '60.603',
        'street_num_int' => 1776,
        'door' => 'BATTLE RD!1776!!1/2!!WEST WINDSOR!08540',
      ]],
    ];
  }

  /**
   * @covers ::buildVoterFields
   * @dataProvider validLineProvider
   */
  public function testValidLine($line, $expected_voter) {
    $v = new VoterZipFileReader('fake.zip');
    $voter = $v->buildVoterFields($line);
    $this->assertSame($expected_voter, $voter);
  }
}
