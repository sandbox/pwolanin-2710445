<?php

/**
 * @file
 * Contains voter.page.inc.
 *
 * Page callback for Voter entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Voter templates.
 *
 * Default template: voter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_voter(array &$variables) {
  // Fetch Voter Entity Object.
  $voter = $variables['elements']['#voter'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
